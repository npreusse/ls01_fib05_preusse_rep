import java.util.Scanner;

public class Fahrkartenautomat {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tastatur = new Scanner(System.in);

		double gesamtbetrag = fahrkartenbestellungErfassen();
		double r�ckgeld = fahrkartenBezahlen(gesamtbetrag);
		fahrkartenAusgeben();
		rueckgeldAusgeben(r�ckgeld);
	}

	public static double fahrkartenbestellungErfassen() {

		Scanner tastatur = new Scanner(System.in);

		System.out.print("Zu zahlender Betrag (EURO): ");
		double zuZahlenderBetrag = tastatur.nextDouble();

		int anzahlderTickets = anzahlTickets(tastatur);

		double gesamtbetrag = anzahlderTickets * zuZahlenderBetrag;
		return gesamtbetrag;
	}
	
	public static int anzahlTickets(Scanner tastatur) {
			
		int anzahlTickets;
		boolean firstRun = true;
		
		do
		{
			if(firstRun == false) {
				System.out.println("Ung�ltiger Wert, die Anzahl muss zwischen 1 und 10 liegen!");
			}
			System.out.print("Anzahl der Tickets: ");
			anzahlTickets = tastatur.nextInt();
			firstRun = false;
		}
		while(!((anzahlTickets > 0) && (anzahlTickets < 11)));
		
		return anzahlTickets;
	}

	public static double fahrkartenBezahlen(double gesamtbetrag) {

		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < gesamtbetrag) {
			System.out.printf("Noch zu zahlen: %.2f Euro\n", (gesamtbetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			double eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		return eingezahlterGesamtbetrag - gesamtbetrag;
	}

	public static void fahrkartenAusgeben() {

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");

	}

	public static void rueckgeldAusgeben(double r�ckgabebetrag) {
		System.out.println("\n\n");

		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f EURO\n", r�ckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt: ");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EUR3O-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05) // 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir w�nschen Ihnen eine gute Fahrt.");
	}
}
