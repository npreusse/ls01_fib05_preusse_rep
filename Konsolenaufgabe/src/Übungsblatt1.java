
public class �bungsblatt1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//"Hallo Welt" ausgeben
		System.out.print("Hallo Welt");
		
		//"Hallo Welt" ausgeben und einen Zeilenvorschub ausf�hren
		System.out.println("Hallo Welt");
		
		//Ausgabe: Er sagte: "Guten Tag!)
		System.out.println("Er sagte: \"Guten Tag!\"");
		
		//"Ich hei�e Nassim und bin 21 Jahre alt." ausgeben
		int alter =  21;
		String name = "Nassim";
		System.out.print("Ich hei�e "+ name + " und bin " + alter + " Jahre alt.");
		
		System.out.printf("Hello %s!%n", "World");
		//Ausgabe: Hello World!
		
		String s = "Java-Programm";
		
		//Standardausgabe 
		System.out.printf( "\n|%s|\n", s);
		
		//rechtsb�ndig mit 20 Stellen
		System.out.printf( "\n|%20s|\n", s);
		
		//linksb�ndig mit 20 Stellen
		System.out.printf( "\n|%-20s|\n", s);
		
		//minimal 5 Stellen
		System.out.printf( "\n|%5s|\n", s);
		
		//maximal 4 Stellen
		System.out.printf( "\n|%.4s|\n", s);

	}

}
